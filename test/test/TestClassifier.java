package test;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import autoseia.SeiaClassifier;
import autoseia.param.hyper.OCVHyperParameters;
import autoseia.param.hyper.MSER;
import autoseia.param.hyper.ORB;


public class TestClassifier {

	public static void main(String[] args) throws Exception {
		
		SeiaClassifier sc;
		sc = new SeiaClassifier();
//		sc.setOptions(new String[]{
//				"-D",FeatureDetector.ORB + "",
//				"-F", "500",
//				"-W","weka.classifiers.bayes.NaiveBayes"});
		
		sc = (SeiaClassifier) weka.core.SerializationHelper.read("/dati/Dropbox/Devil/Library/AutoWEKA/experiments/Toy1/trained.0.model");
		
//		Classifier cl = (Classifier) weka.core.SerializationHelper.read("/home/valentina/Workspace/Library/AutoWEKA/experiments/Toy-2/trained.0.model");
//		int nKpts = 834;
//		HyperParameters fd, de;
//		fd = new MSER();
//		fd.setValue("delta", "14");
//		fd.setValue("minDiversity", "0.2626225397268045"); 
//		fd.setValue("maxVariation", "8.275764174652185"); 
//		fd.setValue("maxEvolution", "524"); 
//		fd.setValue("minMargin", "0.564189363370012"); 
//		fd.setValue("maxArea", "2249"); 
//		fd.setValue("minArea", "50"); 
//		fd.setValue("edgeBlurSize", "3"); 
//		fd.setValue("areaThreshold", "5.730897296419558"); 
//				
//		de = new ORB();
//		de.setValue("WTA_K", "2"); 
//		de.setValue("patchSize", "27"); 
//				
//		fd.setNumKeypoints(nKpts);
//		de.setNumKeypoints(nKpts);
//		
		ArffLoader loader =  new ArffLoader();
		String arff = "/dati/Dropbox/Devil/ToyDataset/dataset/Toy-1/train.arff";
		loader.setFile(new File(arff));
		Instances dataset = loader.getDataSet();
		dataset.setClass(dataset.attribute("class"));
//
//		sc.setAlgorithms(dataset, nKpts, cl, fd, de);
//
////		sc.buildClassifier(dataset);
//		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("test.ser"));
//		oos.writeObject(sc);
//		oos.close();
		
		evaluate(sc, dataset);
		
		System.out.println("Done");
	}

	public static void evaluate(SeiaClassifier sc, Instances dataset)
			throws Exception {
//		sc.buildClassifier(dataset);
		
		System.out.println("Evaluating...");
		int ok = 0;
		for (int i=0; i<dataset.numInstances(); i++) {
			Instance inst = dataset.instance(i);
			if (sc.classifyInstance(inst) == inst.classValue()) {
				ok++;
			}
		}
		System.out.println("tot: "+dataset.numInstances());
		System.out.println("ok:  "+ok);
		System.out.println("%    "+((float)ok/dataset.numInstances()));
	}

}
