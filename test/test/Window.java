package test;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.highgui.Highgui;

import autoseia.ImageFactory;
import autoseia.SeiaFeatureDetector;
import autoseia.SeiaImage;
import autoseia.SeiaKeyPoints;
import autoseia.opencv.OCVFeatureDetector;
import autoseia.opencv.OCVImageFactory;
import autoseia.param.hyper.FAST;
import autoseia.param.hyper.HyperParameters;

public class Window extends javax.swing.JFrame {
	private static final long serialVersionUID = -3890695466057778798L;
	/**
     * Creates new form Window
     */
    public Window() {
        initComponents();
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("autoseia");
        SeiaImage image = new SeiaImage("/dati/Dropbox/Devil/Dataset/Robocup/images/frame-10321.ppm");      
        
        HyperParameters params = new FAST();
//        params.setValue("nonmaxSuppression", "1");
//        params.setValue("threshold", "49");
//        params.setValue("type", "0");
        
        SeiaFeatureDetector fd = params.createFD();
//        fd = FeatureDetector.create(FeatureDetector.PYRAMID_MSER);
        showPoints(image, fd, jLabel1);
        
//        params.setValue("minArea", "3000");
//        fd = params.createFD();
        fd = new OCVFeatureDetector(FeatureDetector.create(FeatureDetector.FAST));
        showPoints(image, fd, jLabel2);
        
        pack();
    }
    
    private void showPoints(SeiaImage image, SeiaFeatureDetector fd, JLabel label) {
        Mat image2 = new Mat();
        
        SeiaKeyPoints keypoints = fd.detect(image);
        //Imgproc.blur(image, image2, new Size(19, 1));
        System.out.println(keypoints.size());
        Features2d.drawKeypoints(image.getSpecificType(Mat.class, OCVImageFactory.getInstance()), 
        		keypoints.getSpecificType(MatOfKeyPoint.class), image2);
        
        MatOfByte bytemat = new MatOfByte();

        Highgui.imencode(".png", image2, bytemat);

        byte[] bytes = bytemat.toArray();
        label.setIcon(new ImageIcon(bytes));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("jLabel1");

        jLabel2.setText("jLabel2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 274, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addContainerGap(273, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Window.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Window().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
