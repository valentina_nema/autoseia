package test;

import java.util.Date;

public class TestNative {
    
    public static void main(String[] args){
        System.loadLibrary("autoseia");
        int loop = 10000000;
        Date start, end;
        start = new Date();
        for(int i = 0; i < loop; i++){
            singleTest();
        }
        end = new Date();
        System.out.println("Tempo single: " + (double)(end.getTime() - start.getTime())/loop);
        
        start = new Date();
        multiTest(loop);
        end = new Date();
        System.out.println("Tempo multi: " + (double)(end.getTime() - start.getTime())/loop);
    }
    
    private static native double singleTest();
    private static native void multiTest(int loop);
}
