package test.comb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import autoseia.Processor;
import autoseia.SeiaInstanceFactory;
import autoseia.param.hyper.HyperParameters;
import autoweka.ApplicabilityTester;
import autoweka.ClassParams;
import autoweka.Util;
import autoweka.ApplicabilityTester.ApplicableClassifiers;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

public class AllCombinations {
	
	public static void main(String[] args) throws FileNotFoundException, Exception {
		
		if(args.length < 5){
			System.out.println("Required args: trainSet, evalSet, fdClass, deClass, logPath");
			return;
		}
		String trainSet = args[0];//"/home/valentina/Workspace/ToyDataset/dataset/Toy-2/train.arff";
		String evalSet = args[1];//"/home/valentina/Workspace/ToyDataset/dataset/Toy-2/eval.arff";
		String fdClass = args[2];
		String deClass = args[3];
		String logPath = args[4];
		
		File logFile = new File(logPath, fdClass + "-" + deClass + ".log");
		logFile.getParentFile().mkdirs();
		PrintWriter logger = new PrintWriter(logFile);
		
		int nKpts = 100;
		SeiaInstanceFactory sif = new SeiaInstanceFactory();
		sif.setNumKeypoints(nKpts);
		String path = "/home/valentina/Workspace/Library/AutoWEKA/params/";
		Map<String, Classifier> classifiers = new HashMap<String, Classifier>();
		
		
		Instances train = Util.loadDataSource(new FileInputStream(trainSet));
		train.setClassIndex(train.numAttributes() - 1);
		Instances eval = Util.loadDataSource(new FileInputStream(evalSet));
		eval.setClassIndex(eval.numAttributes() - 1);

		Processor processor = new Processor();
		sif.setProcessor(processor);
		
		HyperParameters fd, de;
		fd = (HyperParameters) Class.forName("autoseia.param.hyper." + fdClass).newInstance();
		de = (HyperParameters) Class.forName("autoseia.param.hyper." + deClass).newInstance();
		
		fd.setNumKeypoints(nKpts);
		processor.setFeatureDetector(fd);
		
		de.setNumKeypoints(nKpts);
		processor.setDescriptorExtractor(de);
			
		Instances trainMid = sif.convert(train);
		Instances evalMid = sif.convert(eval);
		
		ApplicableClassifiers applClass = ApplicabilityTester.getApplicableClassifiers(trainMid, path, null);
		addClassifiers(applClass.base, classifiers);
		addClassifiers(applClass.ensemble, classifiers);
		addClassifiers(applClass.meta, classifiers);
		classifiers.remove("functions.MultilayerPerceptron");
		
		System.out.println("Loop started");
		for (Entry<String, Classifier> cls : classifiers.entrySet()) {
			try {
				String line = fdClass + "\t" + deClass + "\t" + cls.getKey();
				System.out.println("Learning\t"+line);
				cls.getValue().buildClassifier(trainMid);
				Evaluation e = new Evaluation(evalMid);
				e.evaluateModel(cls.getValue(), evalMid);
				
				double error = 1. - e.errorRate();
				double FN = e.numFalseNegatives(0);
				double FP = e.numFalsePositives(0);
				double TN = e.numTrueNegatives(0); 
				double TP = e.numTruePositives(0);
				
				line += "\t" + error*100 + "\t" + FN + "\t" + FP + "\t" + TN + "\t" + TP;
				logger.println(line);
				System.out.println(line);
				logger.flush();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Done");
		logger.close();
	}

	private static void addClassifiers(List<ClassParams> from, Map<String, Classifier> classifiers) {
		for (ClassParams cp : from) {
			String className = cp.getTargetClass();
			String algoName = className.substring(17);
			
			Classifier cls;
			try {
				cls = (Classifier)Class.forName(className).newInstance();
				classifiers.put(algoName, cls);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
