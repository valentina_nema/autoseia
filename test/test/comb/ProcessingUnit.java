package test.comb;

import weka.classifiers.Classifier;
import weka.core.Instances;

public class ProcessingUnit {
	
	private Instances trainMid, evalMid;
	
	private String fd, de, cls;
	private Classifier c;
	
	public ProcessingUnit(Instances trainMid, Instances evalMid, String fd,
			String de, String cls, Classifier c) {
		this.trainMid = trainMid;
		this.evalMid = evalMid;
		this.fd = fd;
		this.de = de;
		this.cls = cls;
		this.c = c;
	}

	public Instances getTrainMid() {
		return trainMid;
	}

	public Instances getEvalMid() {
		return evalMid;
	}

	public String getFd() {
		return fd;
	}

	public String getDe() {
		return de;
	}

	public String getCls() {
		return cls;
	}

	public Classifier getClassifier() {
		return c;
	}

	
}
