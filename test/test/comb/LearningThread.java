package test.comb;

import java.io.PrintWriter;
import java.util.Deque;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;

import weka.classifiers.Evaluation;

public class LearningThread extends Thread {
	
	private PrintWriter logger;
	private BlockingDeque<ProcessingUnit> deque;
	private boolean done = false;
	
	public LearningThread(PrintWriter logger, BlockingDeque<ProcessingUnit> deque) {
		this.logger = logger;
		this.deque = deque;
	}
	
	public void setDone() {
		done = true;
	}

	@Override
	public void run() {
		while(true) {
			ProcessingUnit u = null;
			try {
				u = deque.pollFirst(2, TimeUnit.SECONDS);
			} catch (InterruptedException e1) {	}
			
			if (u == null) {
				if (done) return;
			} else {
				try {
					String line = u.getFd() + "\t" + u.getDe() + "\t" + u.getCls();
					System.out.println("Learning\t"+line);
					u.getClassifier().buildClassifier(u.getTrainMid());
					Evaluation e = new Evaluation(u.getEvalMid());
					e.evaluateModel(u.getClassifier(), u.getEvalMid());
					double error = e.errorRate();
					double ftrue = e.fMeasure(0);
					double ffalse = e.fMeasure(1);
					line += "\t" + error*100 + "\t" + ftrue + "\t" + ffalse;
					synchronized (logger) {
						logger.println(line);
					}
					System.out.println(line);
					logger.flush();
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
