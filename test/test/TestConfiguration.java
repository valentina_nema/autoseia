package test;

import java.io.File;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.highgui.Highgui;

import autoseia.param.hyper.*;

public class TestConfiguration {
	
	public static void detect(File indir, File outdir, FeatureDetector fd){
		outdir.mkdirs();
		File[] imgs = indir.listFiles();
		for(int i = 0; i < imgs.length; i++){
			Mat image = Highgui.imread(imgs[i].getAbsolutePath());
			if(image.total() > 0){
				MatOfKeyPoint keypoints = new MatOfKeyPoint();
		        Mat image2 = new Mat();
		        fd.detect(image, keypoints);
		        System.out.println(imgs[i].getName() + "\t"+keypoints.total());
		        Features2d.drawKeypoints(image, keypoints, image2);
				Highgui.imwrite(new File(outdir, imgs[i].getName()).getAbsolutePath(), image2);
			}
		}
	}
	
	public static void main(String[] args){
		HyperParameters hp = new BRISK();
		hp.setValue("octaves","1");
		hp.setValue("thres","1");
//		hp.setValue("lineThresholdProjected", "76");
//		hp.setValue("maxSize", "101");
//		hp.setValue("responseThreshold", "241");
//		hp.setValue("suppressNonmaxSize", "10");
		File indir = new File("/dati/Dropbox/Devil/Dataset/Robocup/images/");
//		File indir = new File("/home/valentina/Workspace/ToyDataset/dataset/Toy-4/train/");
//		File indir = new File("/dati/Dropbox/Devil/Dataset/caltech10/images/");
		File outdir = new File("/home/valentina/Thesis/Dataset/RoboCup/testing/");
		FeatureDetector fd;
        //fd = hp.createFD();
        fd = FeatureDetector.create(FeatureDetector.DYNAMIC_STAR);
		detect(indir, outdir, fd);
	}

}
