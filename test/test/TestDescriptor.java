package test;
import org.opencv.core.Core;
import org.opencv.features2d.DescriptorExtractor;

import autoseia.SeiaDescriptorExtractor;
import autoseia.SeiaDescriptors;
import autoseia.SeiaFeatureDetector;
import autoseia.SeiaImage;
import autoseia.SeiaKeyPoints;
import autoseia.param.hyper.BRIEF;
import autoseia.param.hyper.GFTT;
import autoseia.param.hyper.HyperParameters;


public class TestDescriptor {
	
	public static void main(String[] args){
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		System.loadLibrary("autoseia");
		
		createDescriptor();
		
		//writeParams();
		System.out.println("Done");
		
	}

	private static void writeParams() {
		int[] dt = new int[]{DescriptorExtractor.BRIEF,
							DescriptorExtractor.BRISK,
							DescriptorExtractor.FREAK,
							DescriptorExtractor.ORB,
							DescriptorExtractor.SIFT,
							DescriptorExtractor.SURF};
		DescriptorExtractor de;
		for (int i : dt) {
			de = DescriptorExtractor.create(i);
			de.write("params/descriptor"+ i +".txt");
		}
	}

	private static void createDescriptor() {
		String filename = "/dati/Dropbox/Devil/Dataset/Robocup/images/frame-10321.ppm";
		SeiaImage image = new SeiaImage(filename);
		//System.out.println("image size " + image.size().width + " x " + image.size().height);
		
		HyperParameters fdhp;
//		fdhp = new ORB();
//		fdhp.setValue("nLevels", "15");
//		fdhp.setValue("scoreType", "0");
//		fdhp.setValue("edgeThreshold", "40");
//		fdhp.setValue("nFeatures", "875");
//		fdhp.setValue("scaleFactor", "1.0258536003399357");
		fdhp = new GFTT();
		
		SeiaFeatureDetector fd = fdhp.createFD();
		
		SeiaKeyPoints keypoints = fd.detect(image);
		System.out.println("keypoints " + keypoints.size());
		fdhp.normalize(keypoints);
		
		HyperParameters dehp = new BRIEF();//new DummyDescriptor();
		SeiaDescriptorExtractor de = dehp.createDE();
		
		SeiaDescriptors descriptors = de.compute(image, keypoints);
		//System.out.println("descriptors size " + descriptors.numElements().width + " x " + descriptors.numElements().height);
		
		System.out.println(de.getDescriptorSize());
		//System.out.println(de.descriptorType());
		for(int i = 0; i < descriptors.size(); i++){
			double d = descriptors.get(i, 0);
			System.out.println("Componente " + i + ": " + d);
		}
	}

}
