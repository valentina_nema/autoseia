package test;
import java.io.File;
import java.io.IOException;

import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import autoseia.Processor;
import autoseia.SeiaInstance;
import autoseia.SeiaInstanceFactory;
import autoseia.param.hyper.GFTT;
import autoseia.param.hyper.HyperParameters;
import autoseia.param.hyper.SURF;


public class ClassifierInputBuilder {
	
	public static void main(String[] args) throws IOException {
		//build("/home/valentina/Workspace/Library/AutoWEKA/datasets/caltech6.arff");
		build("/home/valentina/Workspace/ToyDataset/dataset/trainRC.arff");
		System.out.println("Training done");
		build("/home/valentina/Workspace/ToyDataset/dataset/evalRC.arff");
		System.out.println("Eval done");
		System.out.println("All Done");
	}

	private static void build(String arff) throws IOException {
		SeiaInstanceFactory sif = new SeiaInstanceFactory();
		sif.setNumKeypoints(100);
		HyperParameters fdParams = new GFTT();
		fdParams.setValue("useHarrisDetector", "1");
//		fdParams.setValue("edgeThreshold", "13");
//		fdParams.setValue("scaleFactor", "1.6024290026597883");
//		fdParams.setValue("scoreType", "0");
		fdParams.setNumKeypoints(sif.getNumKeypoints());
		HyperParameters deParams = new SURF();
//		deParams.setValue("extended", "0");
		deParams.setNumKeypoints(sif.getNumKeypoints());
		Processor processor = new Processor();
		processor.setFeatureDetector(fdParams);
		processor.setDescriptorExtractor(deParams);
		sif.setProcessor(processor);
		
		ArffLoader loader =  new ArffLoader();
		loader.setFile(new File(arff));
		Instances dataset = loader.getDataSet();
		Attribute attr = dataset.attribute("class");
		dataset.setClass(attr);
		sif.setClassAttribute(attr);

		Instances newDataset = sif.createDataset(dataset.numInstances());
		for(int i = 0; i < dataset.numInstances(); i++){
			SeiaInstance si = sif.convert(dataset.instance(i));
			newDataset.add(si);
			//si.writeProcessedImage(si.getFilename().replace(".png", "-P.png"));
		}
		
		ArffSaver saver = new ArffSaver();
		saver.setInstances(newDataset);
		saver.setFile(new File(arff.replace(".arff", "-mid.arff")));
		saver.writeBatch();
	}

}
