package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JFileChooser;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import autoweka.Util;

public class DatasetStat {

	public static void main(String[] args) throws FileNotFoundException, Exception {
		//JFileChooser fc = new JFileChooser();
		//if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
			//calc(fc.getSelectedFile());
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy1/eval.arff"));
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy2/eval.arff"));
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy3/eval.arff"));
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy4/eval.arff"));
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy1/train.arff"));
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy2/train.arff"));
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy3/train.arff"));
		calc(new File("/home/valentina/Dataset/ToyDataset/Toy4/train.arff"));
	}

	public static void calc(File file) throws FileNotFoundException, Exception {
		System.out.println("Reading dataset "+file.getAbsolutePath());
		Instances dataset = Util.loadDataSource(new FileInputStream(file));
		
		int fileIdx = dataset.attribute("filename").index();
		Attribute classAtt = dataset.attribute("class");
		HashMap<String, Integer> classes = new HashMap<>();
		
		@SuppressWarnings("rawtypes")
		Enumeration clVal = classAtt.enumerateValues();
		while(clVal.hasMoreElements()) {
			classes.put(clVal.nextElement().toString(), 0);
		}
		
		for (Instance instance : dataset) {
			String filename = instance.stringValue(fileIdx);
			File img = new File(filename);
			if (!img.isFile())
				System.out.println("Missing file: " + filename);
			
			String iClass = instance.stringValue(classAtt);
			Integer c = classes.get(iClass);
			if (c == null)
				classes.put(iClass, 1);
			else
				classes.put(iClass, c+1);
		}
		
		System.out.println();
		System.out.println("Dataset size: "+dataset.size());
		System.out.println("Classes:");
		for (Entry<String, Integer> e : classes.entrySet()) {
			System.out.println(e.getKey()+"\t"+e.getValue());
		}
	}
}
