package autoseia.param.hyper;

import autoseia.SeiaFeatureDetector;
import autoseia.opencv.OCVFeatureDetector;
import autoseia.param.single.BoolParameter;
import autoseia.param.single.IntParameter;

import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Identifier;

public class FAST extends OCVHyperParameters {

	private static final long serialVersionUID = -7269675464181808747L;
	
	public FAST(){
        params.put("nonmaxSuppression", new BoolParameter(true));
        params.put("threshold", new IntParameter(10));
        params.put("type", new IntParameter(2, 0, 2));
    }

    @Override
    public int getDetectorType() {
        return FeatureDetector.FAST;
    }
    
    @Override
    public SeiaFeatureDetector createFD(){
    	long ptr = fastX();
    	FeatureDetector fd = Identifier.createFD(ptr);
    	update(fd);
    	return new OCVFeatureDetector(fd);
    }
    
	private native long fastX();

	@Override
	public int getExtractorType() {
		return -1;
	}

}
