package autoseia.param.hyper;

import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Identifier;

import autoseia.SeiaFeatureDetector;
import autoseia.opencv.OCVFeatureDetector;
import autoseia.param.single.FloatParameter;
import autoseia.param.single.IntParameter;

public class GFTT extends OCVHyperParameters {

	private static final long serialVersionUID = 2514136003587566497L;

	protected FloatParameter k;
	private FloatParameter minDistance, qualityLevel;
	private IntParameter nfeatures, blockSize;
	protected boolean useHarrisDetector;
	
	public GFTT(){
		k = new FloatParameter(0f);
		minDistance = new FloatParameter(1.0f);
		nfeatures = new IntParameter(1000);
		qualityLevel = new FloatParameter(0.01f);
		blockSize = new IntParameter(3);
		
		
		params.put("minDistance", minDistance);
		params.put("nfeatures", nfeatures);
		params.put("qualityLevel", qualityLevel);
		params.put("blockSize", blockSize);
	}
	
	private native long GoodFeaturesToTrackDetector( int nfeatures, double qualityLevel, 
			double minDistance, int blockSize, boolean useHarrisDetector, double k);
	
	@Override
	public SeiaFeatureDetector createFD() {
		FeatureDetector fd = Identifier.createFD(GoodFeaturesToTrackDetector(nfeatures.getIntValue(), 
				qualityLevel.getValue(), minDistance.getValue(), blockSize.getIntValue(), 
				useHarrisDetector, k.getValue())); 
		return new OCVFeatureDetector(fd);
	}
	
	@Override
	public void setNumKeypoints(int nKeypoints) {
		params.get("nfeatures").setValue(nKeypoints);
	}
	
	@Override
	public int getDetectorType() {
		return FeatureDetector.GFTT;
	}

	@Override
	public int getExtractorType() {
		return -1;
	}
}

/*

class GoodFeaturesToTrackDetector : public FeatureDetector
{
public:
    class Params
    {
    public:
        Params( int maxCorners=1000, double qualityLevel=0.01,
                double minDistance=1., int blockSize=3,
                bool useHarrisDetector=false, double k=0.04 );
        void read( const FileNode& fn );
        void write( FileStorage& fs ) const;

        int maxCorners;
        double qualityLevel;
        double minDistance;
        int blockSize;
        bool useHarrisDetector;
        double k;
    };

    GoodFeaturesToTrackDetector( const GoodFeaturesToTrackDetector::Params& params=
                                            GoodFeaturesToTrackDetector::Params() );
    GoodFeaturesToTrackDetector( int maxCorners, double qualityLevel,
                                 double minDistance, int blockSize=3,
                                 bool useHarrisDetector=false, double k=0.04 );
    virtual void read( const FileNode& fn );
    virtual void write( FileStorage& fs ) const;
protected:
    ...
};

*/