/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autoseia.param.hyper;

import autoseia.param.single.BoolParameter;
import autoseia.param.single.FloatParameter;
import autoseia.param.single.IntParameter;

import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;

/**
 *
 * @author Valentina Nema
 */
public class SURF extends OCVHyperParameters{
 
	private static final long serialVersionUID = 7719590744812777267L;

	public SURF(){
        params.put("hessianThreshold", new FloatParameter(100f));
        params.put("nOctaves", new FloatParameter(4f));
        params.put("nOctaveLayers", new IntParameter(3));
        params.put("extended", new BoolParameter(false));
        params.put("upright", new BoolParameter(false));
    }
    
    @Override
    public int getDetectorType() {
        return FeatureDetector.SURF;
    }

	@Override
	public int getExtractorType() {
		return DescriptorExtractor.SURF;
	}
}
