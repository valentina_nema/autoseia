package autoseia.param.hyper;

import org.opencv.features2d.DescriptorExtractor;

import autoseia.param.single.IntParameter;

public class BRIEF  extends OCVHyperParameters{

	private static final long serialVersionUID = -5590652086552315322L;

	public BRIEF(){
        params.put("bytes", new IntParameter(32, 16, 64));
    }
	
	@Override
	public int getDetectorType() {
		return -1;
	}

	@Override
	public int getExtractorType() {
		return DescriptorExtractor.BRIEF;
	}

}
