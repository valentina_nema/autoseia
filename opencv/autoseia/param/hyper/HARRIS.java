package autoseia.param.hyper;

public class HARRIS extends GFTT {

	private static final long serialVersionUID = 4300771117893329755L;

	public HARRIS(){
		k.setValue(0.04f);
		params.put("k", k);
		useHarrisDetector = true;
	}
}
