/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autoseia.param.hyper;

import autoseia.param.single.FloatParameter;
import autoseia.param.single.IntParameter;
import autoseia.param.single.Parameter;

import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;

/**
 *
 * @author Valentina Nema
 */
public class ORB extends OCVHyperParameters{
    
	private static final long serialVersionUID = 2169278021375308661L;

	public ORB(){
        params.put("nFeatures", new IntParameter(500));
        params.put("scaleFactor", new FloatParameter(1.2f));
        params.put("nLevels", new IntParameter(8));
        params.put("edgeThreshold", new IntParameter(31));
        params.put("firstLevel", new IntParameter(0));
        params.put("WTA_K", new IntParameter(2));
        params.put("scoreType", new IntParameter(0));
        params.put("patchSize", new IntParameter(31));
    }
	
	@Override
	public Parameter getParameter(String name) {
		if(name.equals("WTA-K"))
			name = "WTA_K";
		return super.getParameter(name);
	}
    
	@Override
	public void setValue(String name, String val) {
		if(name.equals("WTA-K"))
			name = "WTA_K";
		super.setValue(name, val);
	}
	
	@Override
	public void setNumKeypoints(int nKeypoints) {
		params.get("nFeatures").setValue(nKeypoints);
	}
	
    @Override
    public int getDetectorType() {
        return FeatureDetector.ORB;
    }

	@Override
	public int getExtractorType() {
		return DescriptorExtractor.ORB;
	}
}
