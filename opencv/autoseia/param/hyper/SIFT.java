package autoseia.param.hyper;

import autoseia.SeiaKeyPoints;
import autoseia.param.single.FloatParameter;
import autoseia.param.single.IntParameter;

import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;

public class SIFT extends OCVHyperParameters{
    
	private static final long serialVersionUID = -3931738485769176973L;
	
	private IntParameter nOctaveLayers;
	private FloatParameter sigma;

	public SIFT(){
		nOctaveLayers = new IntParameter(3);
		sigma = new FloatParameter(1.6f);
        params.put("contrastThreshold", new FloatParameter(0.04f));
        params.put("edgeThreshold", new FloatParameter(10f));
        params.put("nFeatures", new IntParameter(0));
        params.put("nOctaveLayers", nOctaveLayers);
        params.put("sigma", sigma);
    }
    
	@Override
	public void setNumKeypoints(int nKeypoints) {
		params.get("nFeatures").setValue(nKeypoints);
	}
	
    @Override
    public int getDetectorType() {
        return FeatureDetector.SIFT;
    }

	@Override
	public int getExtractorType() {
		return DescriptorExtractor.SIFT;
	}
	
	@Override
	public void normalize(SeiaKeyPoints kpts) {
		MatOfKeyPoint mok = kpts.getSpecificType(MatOfKeyPoint.class);
		float[] kpt = new float[7];
		for(int i = 0; i < mok.rows(); i++){
			mok.get(i, 0, kpt);
			int oct = (int)kpt[5] & 255;
			if (oct >= 128)
				oct = -128 | oct;
			kpt[5] = oct+1;	
			mok.put(i, 0, kpt);
		}
	}
	
	@Override
	public boolean isDetectorAdmissible(HyperParameters fd) {
		if(!isSameAlgorithm(fd))
			return false;
		SIFT sift = (SIFT)fd;
		nOctaveLayers.setValue(sift.nOctaveLayers.getIntValue());
		sigma.setValue(sift.sigma.getFloatValue());
		return true;
	}
}
