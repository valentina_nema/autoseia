/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package autoseia.param.hyper;

import autoseia.param.single.IntParameter;
import org.opencv.features2d.FeatureDetector;

/**
 *
 * @author Valentina Nema
 */
public class STAR extends OCVHyperParameters{
    
	private static final long serialVersionUID = 4574819489704562085L;

	public STAR(){
        params.put("lineThresholdBinarized", new IntParameter(8));
        params.put("lineThresholdProjected", new IntParameter(10));
        params.put("maxSize", new IntParameter(45));
        params.put("responseThreshold", new IntParameter(30));
        params.put("suppressNonmaxSize", new IntParameter(5));
    }
    
    @Override
    public int getDetectorType() {
        return FeatureDetector.STAR;
    }

	@Override
	public int getExtractorType() {
		return -1;
	}
    
}
