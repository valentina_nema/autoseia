package autoseia.param.hyper;

import org.opencv.features2d.DescriptorExtractor;

import autoseia.param.single.BoolParameter;
import autoseia.param.single.FloatParameter;
import autoseia.param.single.IntParameter;

public class FREAK extends OCVHyperParameters {

	private static final long serialVersionUID = -6995139145018995695L;

	public FREAK(){
		params.put("nbOctave", new IntParameter(4));
		params.put("orientationNormalized", new BoolParameter(true));
		params.put("patternScale", new FloatParameter(22));
		params.put("scaleNormalized", new BoolParameter(true));

	}
	
	@Override
	public int getDetectorType() {
		return -1;
	}

	@Override
	public int getExtractorType() {
		return DescriptorExtractor.FREAK;
	}

}
