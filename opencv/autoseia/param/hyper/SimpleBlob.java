package autoseia.param.hyper;

import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Identifier;

import autoseia.SeiaFeatureDetector;
import autoseia.opencv.OCVFeatureDetector;
import autoseia.param.single.BoolParameter;
import autoseia.param.single.FloatParameter;
import autoseia.param.single.IntParameter;

public class SimpleBlob extends OCVHyperParameters {

	private static final long serialVersionUID = 1168467361786245752L;
	
	private FloatParameter thresholdStep, minThreshold, thresholdSize, minDistBetweenBlobs,
						minArea, areaSize, minCircularity, circularitySize, minInertiaRatio,
						inertiaRatioSize, minConvexity, convexitySize;
	private IntParameter minRepeatability, blobColor;
	private BoolParameter filterByColor, filterByArea, filterByCircularity, filterByInertia, filterByConvexity;

	public SimpleBlob(){
		thresholdStep = new FloatParameter(10.0f);
		minThreshold = new FloatParameter(50.0f);
		thresholdSize = new FloatParameter(170.0f);
		minRepeatability = new IntParameter(2);
		minDistBetweenBlobs = new FloatParameter(10.0f);
		filterByColor = new BoolParameter(true);
		blobColor = new IntParameter(0);
		filterByArea = new BoolParameter(true);
		minArea = new FloatParameter(25.0f);
		areaSize = new FloatParameter(4500.0f);
		filterByCircularity = new BoolParameter(false);
		minCircularity = new FloatParameter(0.8f);
		circularitySize = new FloatParameter(0.2f);
		filterByInertia = new BoolParameter(true);
		minInertiaRatio = new FloatParameter(0.1f);
		inertiaRatioSize = new FloatParameter(0.9f);
		filterByConvexity = new BoolParameter(true);
		minConvexity = new FloatParameter(0.95f);
		convexitySize = new FloatParameter(0.05f);
		
		params.put("thresholdStep", thresholdStep);
		params.put("minThreshold", minThreshold);
		params.put("thresholdSize", thresholdSize);
		params.put("minRepeatability", minRepeatability);
		params.put("minDistBetweenBlobs", minDistBetweenBlobs);
		params.put("filterByColor", filterByColor);
		params.put("blobColor", blobColor);
		params.put("filterByArea", filterByArea);
		params.put("minArea", minArea);
		params.put("areaSize", areaSize);
		params.put("filterByCircularity", filterByCircularity);
		params.put("minCircularity", minCircularity);
		params.put("circularitySize", circularitySize);
		params.put("filterByInertia", filterByInertia);
		params.put("minInertiaRatio", minInertiaRatio);
		params.put("inertiaRatioSize", inertiaRatioSize);
		params.put("filterByConvexity", filterByConvexity);
		params.put("minConvexity", minConvexity);
		params.put("convexitySize", convexitySize);
	}
	
	@Override
	public int getDetectorType() {
		return FeatureDetector.SIMPLEBLOB;
	}
	
	private native long SimpleBlobDetector(float thresholdStep,
			float minThreshold, float maxThreshold, float minDistBetweenBlobs,
			float minArea, float maxArea, float minCircularity,
			float maxCircularity, float minInertiaRatio, float maxInertiaRatio,
			float minConvexity, float maxConvexity, int minRepeatability,
			char blobColor, boolean filterByColor, boolean filterByArea,
			boolean filterByCircularity, boolean filterByInertia,
			boolean filterByConvexity);
	
	@Override
	public SeiaFeatureDetector createFD() {
		//checkValues();
		long ptr = SimpleBlobDetector(
				thresholdStep.getFloatValue(),
				minThreshold.getFloatValue(),
				minThreshold.getFloatValue() + thresholdSize.getFloatValue(),
				minDistBetweenBlobs.getFloatValue(),
				minArea.getFloatValue(),
				minArea.getFloatValue() + areaSize.getFloatValue(),
				minCircularity.getFloatValue(),
				circularitySize.getFloatValue(),
				minInertiaRatio.getFloatValue(),
				minInertiaRatio.getFloatValue() + inertiaRatioSize.getFloatValue(),
				minConvexity.getFloatValue(), 
				minConvexity.getFloatValue() + convexitySize.getFloatValue(),
				minRepeatability.getIntValue(), (char) blobColor.getIntValue(),
				filterByColor.getBoolValue(), filterByArea.getBoolValue(),
				filterByCircularity.getBoolValue(),
				filterByInertia.getBoolValue(),
				filterByConvexity.getBoolValue());
		return new OCVFeatureDetector(Identifier.createFD(ptr)); 
	}

	private void checkValues() {
		if(filterByArea.getBoolValue() && areaSize.getFloatValue() < minArea.getFloatValue())
			throw new RuntimeException("minArea greater than maxArea");
		if(filterByCircularity.getBoolValue() && circularitySize.getFloatValue() < minCircularity.getFloatValue())
			throw new RuntimeException("minCircularity greater than maxCircularity");
		if(filterByConvexity.getBoolValue() && convexitySize.getFloatValue() < minConvexity.getFloatValue())
			throw new RuntimeException("minConvexity greater than maxConvexity");
		if(filterByInertia.getBoolValue() && inertiaRatioSize.getFloatValue() < minInertiaRatio.getFloatValue())
			throw new RuntimeException("minInertiaRatio greater than maxInertiaRatio");
	}

	@Override
	public int getExtractorType() {
		return -1;
	}

}
