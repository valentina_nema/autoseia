package autoseia.param.hyper;

import org.opencv.features2d.FeatureDetector;

import autoseia.SeiaFeatureDetector;
import autoseia.param.single.FloatParameter;
import autoseia.param.single.IntParameter;

public class MSER extends OCVHyperParameters {

	private static final long serialVersionUID = 9156826636712834972L;

	public MSER(){
		params.put("areaThreshold", new FloatParameter(1.01f));
		params.put("delta", new IntParameter(5));
		params.put("edgeBlurSize", new IntParameter(5));
		params.put("maxArea", new IntParameter(14400));
		params.put("maxEvolution", new IntParameter(200));
		params.put("maxVariation", new FloatParameter(0.25f));
		params.put("minArea", new IntParameter(60));
		params.put("minDiversity", new FloatParameter(0.2f));
		params.put("minMargin", new FloatParameter(0.003f));
	}
	
	@Override
	public SeiaFeatureDetector createFD() {
		if(params.get("maxArea").getValue() < params.get("minArea").getValue())
			throw new RuntimeException("minArea greater than maxArea");
		return super.createFD();
	}
	
	@Override
	public int getDetectorType() {
		return FeatureDetector.MSER;
	}

	@Override
	public int getExtractorType() {
		return -1;
	}

}
