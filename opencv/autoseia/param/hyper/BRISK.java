package autoseia.param.hyper;

import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;

import autoseia.param.single.IntParameter;

public class BRISK extends OCVHyperParameters{

	private static final long serialVersionUID = 8208652745786765125L;

	public BRISK(){
        params.put("octaves", new IntParameter(3));
        params.put("thres", new IntParameter(30));
    }

    @Override
    public int getDetectorType() {
        return FeatureDetector.BRISK;
    }

	@Override
	public int getExtractorType() {
		return DescriptorExtractor.BRISK;
	}
}
