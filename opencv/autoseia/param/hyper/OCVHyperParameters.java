package autoseia.param.hyper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.opencv.core.Core;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Identifier;

import autoseia.SeiaDescriptorExtractor;
import autoseia.SeiaFeatureDetector;
import autoseia.SeiaKeyPoints;
import autoseia.opencv.OCVDescriptorExtractor;
import autoseia.opencv.OCVFeatureDetector;
import autoseia.param.single.Parameter;

public abstract class OCVHyperParameters implements Serializable, HyperParameters {
	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary("autoseia");
    }

	private static final long serialVersionUID = -1129186722252863597L;
	
	protected Map<String, Parameter> params;
    
    public OCVHyperParameters(){
        params = new HashMap<>();
    }
    
    @Override
	public Parameter getParameter(String name){
        return params.get(name);
    }
    
    @Override
	public void setValue(String name, String val){
        Parameter p = params.get(name);
        if(p != null){
            p.setValue(val);
        }
    }
    
    @Override
	public boolean isSameAlgorithm(HyperParameters hp){
    	return this.getClass().equals(hp.getClass());
    }
    
    @Override
	public void normalize(SeiaKeyPoints kpt){
    	//smile :)
    }

    public abstract int getDetectorType();
    public abstract int getExtractorType();
    
    public SeiaFeatureDetector createFD(){
    	FeatureDetector fd = FeatureDetector.create(getDetectorType());
    	update(fd);
    	return new OCVFeatureDetector(fd);
    }
    
    public SeiaDescriptorExtractor createDE(){
    	DescriptorExtractor de = DescriptorExtractor.create(getExtractorType());
    	update(de);
    	return new OCVDescriptorExtractor(de);
    }
    
    protected void update(FeatureDetector fd){
        update(Identifier.getPtr(fd));
    }

    protected void update(DescriptorExtractor de) {
    	update(Identifier.getPtr(de));
    }
    
	private void update(long ptr) {
		byte cclass = 0;
		if(getDetectorType() >= 0)
			cclass += 1;
		if(getExtractorType() >= 0)
			cclass += 2;
        for (Map.Entry<String, Parameter> entry : params.entrySet()) {
            entry.getValue().update(ptr, entry.getKey(), cclass);
        }
	}
    
    @Override
	public Set<String> getKeys(){
    	return params.keySet();
    }

	@Override
	public void setNumKeypoints(int nKeypoints) {		
	}
	
	@Override
	public boolean isDetectorAdmissible(HyperParameters fd){
		return true;
	}
	
}
