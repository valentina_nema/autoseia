package autoseia.opencv;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.FeatureDetector;

import autoseia.SeiaFeatureDetector;
import autoseia.SeiaImage;
import autoseia.SeiaKeyPoints;

public class OCVFeatureDetector implements SeiaFeatureDetector {
	
	private FeatureDetector fd;
	
	public OCVFeatureDetector(FeatureDetector fd){
		this.fd = fd;
	}

	@Override
	public SeiaKeyPoints detect(SeiaImage image) {
		Mat mat = image.getSpecificType(Mat.class, OCVImageFactory.getInstance());
		MatOfKeyPoint mok = new MatOfKeyPoint();
		fd.detect(mat, mok);
		return new OCVKeyPoints(mok);
	}

}
