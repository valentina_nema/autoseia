package autoseia.opencv;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import autoseia.ImageFactory;
import autoseia.ImageInfo;
import autoseia.SeiaImage;

public class OCVImageFactory implements ImageFactory<Mat> {
	
	private static OCVImageFactory me = null;
	
	private OCVImageFactory(){
		
	}

	public static OCVImageFactory getInstance(){
		if(me == null)
			me = new OCVImageFactory();
		return me;
	}
	
	@Override
	public Mat create(SeiaImage image) {
		
		return Highgui.imread(image.getFilename());
	}

	@Override
	public ImageInfo createInfo(SeiaImage seiaimage, final Mat image) {
		return new ImageInfo(){

			@Override
			public int[] getRGB(int x, int y) {
				double[] trgb = image.get(y, x);
				int[] rgb = new int[trgb.length];
				for(int i = 0; i < rgb.length; i++)
					rgb[i] = (int)trgb[i];
				return rgb;
			}
			
		};
	}

}
