package autoseia.opencv;

import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Point;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.KeyPoint;

import autoseia.SeiaDescriptorExtractor;
import autoseia.SeiaDescriptors;
import autoseia.SeiaImage;
import autoseia.SeiaKeyPoints;

public class OCVDescriptorExtractor implements SeiaDescriptorExtractor {

	private DescriptorExtractor de;
	
	public OCVDescriptorExtractor(DescriptorExtractor de){
		this.de = de;
	}
	
	@Override
	public int getDescriptorSize() {
		return de.descriptorSize();
	}

	@Override
	public SeiaDescriptors compute(SeiaImage image, SeiaKeyPoints keyPoints) {
		Mat mat = image.getSpecificType(Mat.class, OCVImageFactory.getInstance());
		
		MatOfKeyPoint mok = keyPoints.getSpecificType(MatOfKeyPoint.class);
		if(mok == null){
			mok = this.convert(keyPoints);
		}
		Mat descriptors = new Mat();
		de.compute(mat, mok, descriptors);
		return new OCVDescriptors(descriptors);

	}
	
	public MatOfKeyPoint convert(SeiaKeyPoints skp){
		KeyPoint[] kps = new KeyPoint[skp.size()];
		for(int i = 0; i < kps.length; i++){
			KeyPoint kp = new KeyPoint();
			kp.angle = skp.getAngle(i);
			kp.octave = skp.getOctave(i);
			kp.pt = new Point(skp.getX(i), skp.getY(i));
			kp.response = skp.getResponse(i);
			kp.size = skp.getSize(i);
		}
		return new MatOfKeyPoint(kps);
	}

}
