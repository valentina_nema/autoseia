package autoseia.opencv;

import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.KeyPoint;

import autoseia.SeiaKeyPoints;

public class OCVKeyPoints implements SeiaKeyPoints {
	
	private MatOfKeyPoint mok;
	private KeyPoint[] kps;

	public OCVKeyPoints(MatOfKeyPoint mok){
		this.mok = mok;
		kps = mok.toArray();
	}
	
	@Override
	public float getSize(int index) {
		return kps[index].size;
	}

	@Override
	public float getAngle(int index) {
		return kps[index].angle;
	}

	@Override
	public float getResponse(int index) {
		return kps[index].response;
	}

	@Override
	public int getOctave(int index) {
		return kps[index].octave;
	}

	@Override
	public int getX(int index) {
		return (int)kps[index].pt.x;
	}

	@Override
	public int getY(int index) {
		return (int)kps[index].pt.y;
	}

	@Override
	public int size() {
		return kps.length;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getSpecificType(Class<T> type) {
		if(MatOfKeyPoint.class.equals(type)){
			return (T)mok;
		}
		return null;
	}

}
