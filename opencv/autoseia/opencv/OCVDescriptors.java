package autoseia.opencv;

import org.opencv.core.Mat;

import autoseia.SeiaDescriptors;

public class OCVDescriptors implements SeiaDescriptors {

	private Mat descriptors;
	
	public OCVDescriptors(Mat mat){
		descriptors = mat;
	}
	
	@Override
	public double get(int index, int elem) {
		return descriptors.get(index, elem)[0];
	}

	@Override
	public int numElements() {
		return descriptors.cols();
	}

	@Override
	public int size() {
		return descriptors.rows();
	}

}
