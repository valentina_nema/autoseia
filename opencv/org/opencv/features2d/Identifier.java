/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.opencv.features2d;

/**
 *
 * @author Valentina Nema
 */
public class Identifier {
	
    public static long getPtr(FeatureDetector fd) {
        return fd.nativeObj;
    }
    
    public static long getPtr(DescriptorExtractor de) {
        return de.nativeObj;
    }
    
    public static FeatureDetector createFD(long ptr){
    	return new FeatureDetector(ptr);
    }
}
