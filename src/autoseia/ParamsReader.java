package autoseia;

import java.util.Properties;

import autoseia.param.hyper.HyperParameters;

public class ParamsReader {
	
	private HyperParameters fdParams, deParams;
	private int nKeypoints;
	
	public ParamsReader(String[] options) throws Exception{
		this(array2props(options));
	}
	
	private static Properties array2props(String[] a){
		Properties p = new Properties();
		for(int i = 0; i < a.length; i += 2){
			p.setProperty(a[i].substring(1), a[i+1]);
		}
		return p;
	}
	
	public ParamsReader(Properties props) throws Exception{
		
		nKeypoints = getIntOption("K", props, 0);
		
		String fdType = props.getProperty("FD");
		String deType = props.getProperty("DE");
		
		if (fdType == null) 
			throw new IllegalArgumentException("Invalid FD type "+fdType);
		if (deType == null) 
			throw new IllegalArgumentException("Invalid DE type "+deType);
		
		fdParams = (HyperParameters) Class.forName("autoseia.param.hyper." + fdType).newInstance();
		deParams = (HyperParameters) Class.forName("autoseia.param.hyper." + deType).newInstance();
		
		setParamOptions(props, fdParams, "fd-");
		setParamOptions(props, deParams, "de-");		
	}
	
	public int getNumKeypoints() {
		return nKeypoints;
	}

	public HyperParameters getFdParams() {
		return fdParams;
	}

	public HyperParameters getDeParams() {
		return deParams;
	}
	
	private int getIntOption(String flag, Properties props, int def){
		String strval = props.getProperty(flag);
		if(strval == null)
			return def;
		try{
			return Integer.parseInt(strval);
		} catch(NumberFormatException e){
			return def;
		}
	}
	
	private void setParamOptions(Properties props, HyperParameters params, String prefix){
		if(params != null){
			
			for (String k : params.getKeys()) {
				String opt = props.getProperty(prefix + k);
				if( opt != null)
					params.setValue(k, opt);
			}
			params.setNumKeypoints(nKeypoints);
		}
	}

	
}
