package autoseia;

import weka.classifiers.Evaluation;
import weka.core.Instances;
import autoweka.ClassifierResult.Metric;

public class HarmonicErrorRateMetric implements Metric {
    public float getDefault() { return 100; }
    public float getScore(Evaluation eval, Instances testingData){
    	int n = testingData.classAttribute().numValues();
    	float d = 0;
    	for(int i=0; i<n; i++)
    		d += 1f/eval.recall(i);
        return (float)(100*(1 - n/d)); 
    } 
}
