package autoseia;

public interface DescriptorSizeListener {
	
	void setDescriptorSize(int size);

}
