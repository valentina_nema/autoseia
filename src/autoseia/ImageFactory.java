package autoseia;

public interface ImageFactory<T> {
	
	public abstract T create(SeiaImage image);
	
	public abstract ImageInfo createInfo(SeiaImage seiaimage, T image);

}
