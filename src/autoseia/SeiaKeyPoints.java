package autoseia;

public interface SeiaKeyPoints {

	public abstract float getSize(int index);

	public abstract float getAngle(int index);

	public abstract float getResponse(int index);

	public abstract int getOctave(int index);
	
	public abstract int getX(int index);
	
	public abstract int getY(int index);
	
	public abstract int size();
	
	public abstract <T> T getSpecificType(Class<T> type);

}