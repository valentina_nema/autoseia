package autoseia;

public class EmptyDescriptors implements SeiaDescriptors {

	private static EmptyDescriptors me = null;
	
	public static EmptyDescriptors getInstance(){
		if(me == null)
			me = new EmptyDescriptors();
		return me;
	}
	
	private EmptyDescriptors(){
		
	}
	
	@Override
	public double get(int index, int elem) {
		return 0;
	}

	@Override
	public int numElements() {
		return 0;
	}

	@Override
	public int size() {
		return 0;
	}

}
