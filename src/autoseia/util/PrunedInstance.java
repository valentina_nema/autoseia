package autoseia.util;

import java.util.ArrayList;
import java.util.Enumeration;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class PrunedInstance implements Instance {
	private Instance base;
	private int max;
	private Instances dataset = null;
	private boolean firstCopy = true;
	
	public static Instances prune(Instances base, int maxAttr) {
		int baseAttr = base.numAttributes();
		if (maxAttr >= baseAttr-1) return base;
		int baseClass = base.classIndex();
		if (baseClass == baseAttr-1 && maxAttr >= baseAttr-2)
			return base;
		
		ArrayList<Attribute> attr = new ArrayList<>(maxAttr+2);
		for(int i=0; i<=maxAttr; i++){
			attr.add(base.attribute(i));
		}
		int classIndex;
		if (baseClass > maxAttr) {
			Attribute newClassAttr;
			Attribute baseClassAttr = base.classAttribute();
			@SuppressWarnings("rawtypes")
			Enumeration val = baseClassAttr.enumerateValues();
			if (val == null)
				newClassAttr = new Attribute(baseClassAttr.name(), attr.size());
			else {
				ArrayList<String> attVal = new ArrayList<>();
				while(val.hasMoreElements())
					attVal.add(val.nextElement().toString());
				newClassAttr = new Attribute(baseClassAttr.name(), attVal, attr.size());
			}
			attr.add(newClassAttr);
			classIndex = newClassAttr.index();
		} else {
			classIndex = baseClass;
		}
		
		Instances pruned = new Instances(base.relationName(), attr, base.size());
		pruned.setClassIndex(classIndex);
		for (Instance instance : base) {
			pruned.add(new PrunedInstance(instance, maxAttr, false));
		}
		return pruned;
	}
	
	
	private PrunedInstance(Instance base, int max, boolean copied) {
		this.base = base;
		this.max = max;
		this.firstCopy = !copied;
	}

	private int convert(int index) {
		if (index <= max) return index;
		if (dataset != null && index == dataset.classIndex())
			return base.classIndex();
		throw new IndexOutOfBoundsException();
	}

	@Override
	public Object copy() {
		if (firstCopy) {
			firstCopy = false;
			return this;
		} else
			return new PrunedInstance(base, max, true);
	}

	@Override
	public Attribute attribute(int index) {
		return dataset.attribute(index);
	}

	@Override
	public Attribute attributeSparse(int indexOfIndex) {
		return attribute(indexOfIndex);
	}

	@Override
	public Attribute classAttribute() {
		return base.classAttribute();
	}

	@Override
	public int classIndex() {
		return dataset.classIndex();
	}

	@Override
	public boolean classIsMissing() {
		return base.classIsMissing();
	}

	@Override
	public double classValue() {
		return base.classValue();
	}

	@Override
	public Instances dataset() {
		return dataset;
	}

	@Override
	public void deleteAttributeAt(int position) {
		base.deleteAttributeAt(convert(position));
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Enumeration enumerateAttributes() {
		return dataset.enumerateAttributes();
	}

	@Override
	public boolean equalHeaders(Instance inst) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String equalHeadersMsg(Instance inst) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasMissingValue() {
		for (int i = 0; i <= max; i++) {
			if (base.isMissing(i)) return true;
		}
		return base.classIsMissing();
	}

	@Override
	public int index(int position) {
		return position;
	}

	@Override
	public void insertAttributeAt(int position) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isMissing(int attIndex) {
		return base.isMissing(convert(attIndex));
	}

	@Override
	public boolean isMissingSparse(int indexOfIndex) {
		return isMissing(indexOfIndex);
	}

	@Override
	public boolean isMissing(Attribute att) {
		return isMissing(att.index());
	}

	@Override
	public Instance mergeInstance(Instance inst) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int numAttributes() {
		return dataset.numAttributes();
	}

	@Override
	public int numClasses() {
		return base.numAttributes();
	}

	@Override
	public int numValues() {
		return dataset.numAttributes();
	}

	@Override
	public void replaceMissingValues(double[] array) {
		base.replaceMissingValues(array);
	}

	@Override
	public void setClassMissing() {
		base.setClassMissing();
	}

	@Override
	public void setClassValue(double value) {
		base.setClassValue(value);
	}

	@Override
	public void setClassValue(String value) {
		base.setClassValue(value);
	}

	@Override
	public void setDataset(Instances instances) {
		dataset = instances;
	}

	@Override
	public void setMissing(int attIndex) {
		base.setMissing(attIndex);
	}

	@Override
	public void setMissing(Attribute att) {
		base.setMissing(att);
	}

	@Override
	public void setValue(int attIndex, double value) {
		setValue(attIndex, value);
	}

	@Override
	public void setValueSparse(int indexOfIndex, double value) {
		base.setValueSparse(indexOfIndex, value);
	}

	@Override
	public void setValue(int attIndex, String value) {
		base.setValue(attIndex, value);
	}

	@Override
	public void setValue(Attribute att, double value) {
		base.setValue(att, value);
	}

	@Override
	public void setValue(Attribute att, String value) {
		base.setValue(att, value);
	}

	@Override
	public void setWeight(double weight) {
		base.setWeight(weight);
	}

	@Override
	public Instances relationalValue(int attIndex) {
		return base.relationalValue(convert(attIndex));
	}

	@Override
	public Instances relationalValue(Attribute att) {
		return relationalValue(att.index());
	}

	@Override
	public String stringValue(int attIndex) {
		return base.stringValue(convert(attIndex));
	}

	@Override
	public String stringValue(Attribute att) {
		return stringValue(att.index());
	}

	@Override
	public double[] toDoubleArray() {
		double[] arr = new double[numAttributes()];
		for (int i = 0; i < max; i++) {
			arr[i] = base.value(i);
		}
		if (arr.length > max)
			arr[arr.length-1] = base.classValue();
		return arr;
	}

	@Override
	public String toStringNoWeight(int afterDecimalPoint) {
		return base.toStringNoWeight(afterDecimalPoint);
	}

	@Override
	public String toStringNoWeight() {
		return base.toStringNoWeight();
	}

	@Override
	public String toStringMaxDecimalDigits(int afterDecimalPoint) {
		return base.toStringMaxDecimalDigits(afterDecimalPoint);
	}

	@Override
	public String toString(int attIndex, int afterDecimalPoint) {
		return base.toString(convert(attIndex), afterDecimalPoint);
	}

	@Override
	public String toString(int attIndex) {
		return base.toString(convert(attIndex));
	}

	@Override
	public String toString(Attribute att, int afterDecimalPoint) {
		return toString(att.index(), afterDecimalPoint);
	}

	@Override
	public String toString(Attribute att) {
		return toString(att.index());
	}

	@Override
	public double value(int attIndex) {
		return base.value(convert(attIndex));
	}

	@Override
	public double valueSparse(int indexOfIndex) {
		return value(indexOfIndex);
	}

	@Override
	public double value(Attribute att) {
		return value(att.index());
	}

	@Override
	public double weight() {
		return base.weight();
	}

}
