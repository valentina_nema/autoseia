package autoseia.util;

import java.util.List;

import autoseia.param.hyper.HyperParameters;

public class ParamsBuilder {

	public static HyperParameters build(String type) {
		try {
			return (HyperParameters)Class.forName("autoseia.param.hyper." + type).newInstance();
        }	
		catch(ClassNotFoundException e)
        {
            throw new RuntimeException("Could not find class '" + type + "': " + e.getMessage(), e);
        }
        catch(Exception e)
        {
            throw new RuntimeException("Failed to instantiate '" + type + "': " + e.getMessage(), e);
        }
	}

	public static HyperParameters build(String type, List<String> args) {
		HyperParameters params = build(type);
		
		for(int i = 0; i < args.size(); i += 2){
			params.setValue(args.get(i).substring(1), args.get(i+1));
		}
		return params;
	}
}
