package autoseia;

public class EmptyKeyPoints implements SeiaKeyPoints {


	private static EmptyKeyPoints me = null;
	
	public static EmptyKeyPoints getInstance(){
		if(me == null)
			me = new EmptyKeyPoints();
		return me;
	}
	
	private EmptyKeyPoints(){
		
	}
	
	@Override
	public float getSize(int index) {
		return 0;
	}

	@Override
	public float getAngle(int index) {
		return 0;
	}

	@Override
	public float getResponse(int index) {
		return 0;
	}

	@Override
	public int getOctave(int index) {
		return 0;
	}

	@Override
	public int getX(int index) {
		return 0;
	}

	@Override
	public int getY(int index) {
		return 0;
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public <T> T getSpecificType(Class<T> type) {
		return null;
	}

}
