package autoseia;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Random;

import autoseia.param.hyper.HyperParameters;

public class Processor implements Serializable {
	
	private static final long serialVersionUID = 7899301881069326529L;
	
	private transient SeiaFeatureDetector fDetector;
	private transient SeiaDescriptorExtractor dExtractor;
	private HyperParameters featureParams, descriptorParams;
	private DescriptorSizeListener listener;
	
	public void setListener(DescriptorSizeListener listener) {
		this.listener = listener;
		if(dExtractor != null)
			listener.setDescriptorSize(dExtractor.getDescriptorSize());
	}

	public void setFeatureDetector(HyperParameters hp){
		featureParams = hp;
		if(featureParams != null){
			fDetector = featureParams.createFD(); 
		}
		else fDetector = null;
	}
	
	public void setDescriptorExtractor(HyperParameters hp){
		descriptorParams = hp;
		if(descriptorParams != null){
			dExtractor = descriptorParams.createDE();
			if(listener != null)
				listener.setDescriptorSize(dExtractor.getDescriptorSize());
		}
		else {
			dExtractor = null;
			if(listener != null)
				listener.setDescriptorSize(-1);
		}
	}

	public HyperParameters getHyperParams() {
		return featureParams;
	}

	public KeypointDescriptorList process(String src, Random rnd) {
		SeiaImage image = new SeiaImage(src);
		return process(image, rnd);
	}
	
	public KeypointDescriptorList process(SeiaImage image, Random rnd) {
		if(fDetector == null || dExtractor == null){
			return new KeypointDescriptorList();
		}
		SeiaKeyPoints keyPoints = fDetector.detect(image);
		
		//TODO
		/*
		if (!featureParams.isSameAlgorithm(descriptorParams)) {
			featureParams.normalize(matofkeyp);
		}*/
		SeiaDescriptors descriptors = dExtractor.compute(image, keyPoints);
		return new KeypointDescriptorList(keyPoints, descriptors, rnd);
	}

	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		setFeatureDetector(featureParams);
		setDescriptorExtractor(descriptorParams);
	}

	public HyperParameters getFeatureDetector() {
		return featureParams;
	}
	
	public HyperParameters getDescriptorExtractor() {
		return descriptorParams;
	}
}
