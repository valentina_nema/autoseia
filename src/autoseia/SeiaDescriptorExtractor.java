package autoseia;

public interface SeiaDescriptorExtractor {

	public abstract SeiaDescriptors compute(SeiaImage image, SeiaKeyPoints keyPoints);
	
	public abstract int getDescriptorSize();

}
