package autoseia;

public interface ImageInfo {
	
	public abstract int[] getRGB(int x, int y);

}
