package autoseia;

public interface SeiaFeatureDetector {

	public abstract SeiaKeyPoints detect(SeiaImage image);
		
}
