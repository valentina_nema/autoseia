package autoseia;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class SeiaInstanceFactory implements Serializable, DescriptorSizeListener {
	private static final long serialVersionUID = -1347219704975398224L;

	private int CLASS = 0;
	
	private static final int ANGLE = 0;
	private static final int OCTAVE = 1;
	private static final int PTX = 2;
	private static final int PTY = 3;
	private static final int SIZE = 4;
	private static final int CH1 = 5;
	private static final int CH2 = 6;
	private static final int CH3 = 7;
	private static final int DSTART = 8; //descriptor start
	
	private static final int nKpAttr = 8;

	private int nKeypoints = -1;
	private int descriptorSize = -1;
	private transient ArrayList<Attribute> attributes;
	private Attribute classAttr;
	private Processor processor;
	private Random rnd = null;
	
	public SeiaInstanceFactory(){
		buildAttributes(nKpAttr, 1, 0);
	}
	
	public void setSeed(long seed) {
		rnd = new Random(seed);
	}
	
	public Processor getProcessor() {
		return processor;
	}

	public void setProcessor(Processor p) {
		processor = p;
		processor.setListener(this);
	}

	public int getNumKeypoints() {
		return nKeypoints;
	}
	
	public void setNumKeypoints(int nKeypoints) {
		this.nKeypoints = nKeypoints;
		buildAttributes();
	}

	public int getDescriptorSize() {
		return descriptorSize;
	}

	public void setDescriptorSize(int descriptorSize) {
		this.descriptorSize = descriptorSize;
		buildAttributes();
	}
	
	private int nAttr() {
		return nKpAttr+descriptorSize;
	}

	private void buildAttributes(){
		if(nKeypoints < 0)
			return;
		if(descriptorSize < 0)
			return;
		buildAttributes(nAttr(), nKeypoints, descriptorSize);
	}
	
	private void buildAttributes(int nAttr, int nkp, int descSize) {
		
		attributes = new ArrayList<Attribute>(nAttr*nkp + 1);
		CLASS = nAttr*nkp;
		for(int i = 0; i < nkp; i++){
			int j = i*nAttr;
			attributes.add(new Attribute("angle" + i, ANGLE + j));
			attributes.add(new Attribute("octave" + i, OCTAVE + j));
			attributes.add(new Attribute("x" + i, PTX + j));
			attributes.add(new Attribute("y" + i, PTY + j));
			attributes.add(new Attribute("size" + i, SIZE + j));
			attributes.add(new Attribute("ch1_" + i, CH1 + j));
			attributes.add(new Attribute("ch2_" + i, CH2 + j));
			attributes.add(new Attribute("ch3_" + i, CH3 + j));
			for(int d = 0; d < descSize; d++){
				attributes.add(new Attribute("d" + d + "k" + i, DSTART + d + j));
			}
		}
		attributes.add(new Attribute("class", CLASS));
	}
	
	public void setClassAttribute(Attribute a) {
		classAttr = a.copy("class");
		attributes.set(CLASS, classAttr);
	}
	
	public Instances convert(Instances src){
		setClassAttribute(src.classAttribute());
		Instances newDataset = createDataset(src.numInstances());
		int filenameIndex = src.attribute("filename").index();
		for(int i = 0; i < src.numInstances(); i++){
			newDataset.add(convert(src.get(i), filenameIndex));
		}
		return newDataset;
	}
	
	public SeiaInstance convert(Instance src){
		int filenameIndex = src.dataset().attribute("filename").index();
		return convert(src, filenameIndex);
	}

	private SeiaInstance convert(Instance src, int filenameIndex) {
		String filename = src.stringValue(filenameIndex);
		SeiaInstance newInst;
		SeiaImage image = new SeiaImage(filename);
		newInst = new SeiaInstance(attributes.size(), nKeypoints, filename, image, processor, rnd);
		setValues(newInst, image);
		setClass(src, newInst);
		return newInst;
	}
	
	private void setValues(SeiaInstance instance, SeiaImage image){
		KeypointDescriptorList features = instance.getFeatures();
		
		for(int j = 0; j < features.size(); j++){
			int jn = j * nAttr();
			int x = features.getX(j);
			int y = features.getY(j);
			instance.setValue(ANGLE + jn, features.getAngle(j));
			instance.setValue(OCTAVE + jn, features.getOctave(j));
			instance.setValue(PTX + jn, x);
			instance.setValue(PTY + jn, y);
			instance.setValue(SIZE + jn, features.getSize(j));
			int[] temp = image.getRGB(x, y);
			instance.setValue(CH1 + jn, temp[0]);
			instance.setValue(CH2 + jn, temp[1]);
			instance.setValue(CH3 + jn, temp[2]);
			for(int d = 0; d < descriptorSize; d++){
				instance.setValue(DSTART + d + jn, features.getDescriptorElement(j, d));
			}
		}
		
		for(int j = features.size(); j < nKeypoints; j++){
			int jn = j * nAttr();
			instance.setValue(ANGLE + jn, -1);
			instance.setValue(OCTAVE + jn, -1);
			instance.setValue(PTX + jn, -1);
			instance.setValue(PTY + jn, -1);
			instance.setValue(SIZE + jn, -1);
			instance.setValue(CH1 + jn, -1);
			instance.setValue(CH2 + jn, -1);
			instance.setValue(CH3 + jn, -1);
			for(int d = 0; d < descriptorSize; d++){
				instance.setValue(DSTART + d + jn, -1);
			}
		}
	}
	
	private void setClass(Instance src, Instance dst){
		if (src.classIsMissing())
			dst.setMissing(CLASS);
		else {
			dst.setValue(CLASS, src.classValue());
		}
	}
	
	public Instances createDataset(int numInstances){
		Instances data = new Instances("autoseia", attributes, numInstances);
		data.setClassIndex(CLASS);
		return data;
	}
	
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		buildAttributes();
		if(classAttr != null)
			attributes.set(CLASS, classAttr);
	} 
	
	public int getUsedAttributes(Instances dataset){
		int max = PTX;
		int n = nAttr();
		for (Instance i : dataset) {
			while(max < i.numAttributes() && i.value(max) != -1){
				max = max + n;
			}
			if(max >= i.numAttributes())
				return i.numAttributes();
		}
		return max - PTX;
	}

}
