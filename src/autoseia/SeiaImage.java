package autoseia;

import java.util.HashMap;
import java.util.Map;

public class SeiaImage implements ImageInfo{
	
	private String filename;
	private ImageInfo info = null;
	private Map<Class<?>, Object> typeMap = new HashMap<>(); 

	public SeiaImage(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getSpecificType(Class<T> type, ImageFactory<T> factory) {
		
		if(typeMap.containsKey(type))
			return (T)typeMap.get(type);
		
		T impl = factory.create(this);
		
		if(info == null)
			info = factory.createInfo(this, impl);
		typeMap.put(type, impl);
		return impl;
	}

	@Override
	public int[] getRGB(int x, int y) {
		return info.getRGB(x, y);
	}

}
