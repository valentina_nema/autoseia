package autoseia;

public interface SeiaDescriptors {

	double get(int index, int elem);

	int numElements();

	int size();

}
