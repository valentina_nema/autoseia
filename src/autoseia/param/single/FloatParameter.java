package autoseia.param.single;

public class FloatParameter implements Parameter{

	private static final long serialVersionUID = -5389812571004889994L;
	private float value;
	private float def;
	
    public FloatParameter(float value, float min, float max) {
    	this(value);
    }
    
    public FloatParameter(float value){
        this.value = value;
        def = value;
    }

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public void setValue(String str) {
		try{
			value = Float.parseFloat(str);
		} catch(NumberFormatException e){
			value = def;
		}
	}
    
	public float getFloatValue(){
		return value;
	}


	@Override
	public void update(long ptr, String key, byte cclass) {
		setFloat(ptr, key, value, cclass);
	}

	private native void setFloat(long ptr, String key, double val, byte cclass);

	@Override
	public void setValue(int v) {
		value = v;
	}

	@Override
	public void setValue(float v) {
		value = v;
	}
	
    
}
