package autoseia.param.single;

public class IntParameter implements Parameter{

	private static final long serialVersionUID = -1332582441340051290L;
	private int value;
	private int def;
	
    public IntParameter(int value, int min, int max) {
    	this(value);
    }
    
    public IntParameter(int value){
        this.value = value;
        def = value;
    }

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public void setValue(String str) {
		try{
			value = Integer.parseInt(str);
		} catch(NumberFormatException e){
			value = def;
		}
	}
    
	public int getIntValue(){
		return value;
	}

	@Override
	public void update(long ptr, String key, byte cclass) {
		setInt(ptr, key, value, cclass);
	}

	private native void setInt(long ptr, String key, int val, byte cclass);

	@Override
	public void setValue(int v) {
		value = v;
	}

	@Override
	public void setValue(float v) {
		value = (int) v;
	}
}
