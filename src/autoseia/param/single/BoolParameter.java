package autoseia.param.single;

public class BoolParameter extends IntParameter{
        
	private static final long serialVersionUID = -2756742667810284422L;

	public BoolParameter(boolean value){
        super(value ? 1 : 0, 0, 1);
    }
	
	public boolean getBoolValue(){
		return getIntValue() == 1;
	}
}
