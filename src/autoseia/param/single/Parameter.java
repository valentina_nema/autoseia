package autoseia.param.single;

import java.io.Serializable;

public interface Parameter extends Serializable {
	
    double getValue();
    
    void setValue(String str);
    
    void setValue(int v);
    void setValue(float v);
    
	void update(long ptr, String key, byte cclass);
        
}
