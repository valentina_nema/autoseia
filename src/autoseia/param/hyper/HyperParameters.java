package autoseia.param.hyper;

import java.io.Serializable;
import java.util.Set;

import autoseia.SeiaDescriptorExtractor;
import autoseia.SeiaFeatureDetector;
import autoseia.SeiaKeyPoints;
import autoseia.param.single.Parameter;

public interface HyperParameters extends Serializable{

	public abstract Parameter getParameter(String name);

	public abstract void setValue(String name, String val);

	public abstract boolean isSameAlgorithm(HyperParameters hp);

	public abstract void normalize(SeiaKeyPoints kpt);

	public abstract Set<String> getKeys();

	public abstract void setNumKeypoints(int nKeypoints);

	public abstract boolean isDetectorAdmissible(HyperParameters fd);

	public abstract SeiaFeatureDetector createFD();

	public abstract SeiaDescriptorExtractor createDE();

}