package autoseia.param.hyper;

import java.util.Collections;
import java.util.Set;

import autoseia.EmptyDescriptors;
import autoseia.SeiaDescriptorExtractor;
import autoseia.SeiaDescriptors;
import autoseia.SeiaFeatureDetector;
import autoseia.SeiaImage;
import autoseia.SeiaKeyPoints;
import autoseia.param.single.Parameter;

public class DummyDescriptor implements HyperParameters {

	private static final long serialVersionUID = -5427240352522882538L;
	private int deSize;
	
	public DummyDescriptor(int s){
		deSize = s;
	}
	
	public DummyDescriptor(){
		this(0);
	}

	@Override
	public SeiaDescriptorExtractor createDE() {
		return new DummyDE(deSize);
	}
	
	private static class DummyDE implements SeiaDescriptorExtractor {

		private int size;
		
		protected DummyDE(int s) {
			size = s;
		}
		
		@Override
		public int getDescriptorSize() {
			return size;
		}
		
		@Override
		public SeiaDescriptors compute(SeiaImage image, SeiaKeyPoints keypoints) {
			
			if(size == 0)
				return EmptyDescriptors.getInstance();
			
			return new SeiaDescriptors() {
				
				@Override
				public int numElements() {
					return size;
				}
				
				@Override
				public double get(int index, int elem) {
					return 0;
				}

				@Override
				public int size() {
					return 0;
				}
			};
		}
		
	}

	@Override
	public Parameter getParameter(String name) {
		return null;
	}

	@Override
	public void setValue(String name, String val) {
	}

	@Override
	public boolean isSameAlgorithm(HyperParameters hp) {
		return false;
	}

	@Override
	public Set<String> getKeys() {
		return Collections.emptySet();
	}

	@Override
	public void setNumKeypoints(int nKeypoints) {
	}

	@Override
	public boolean isDetectorAdmissible(HyperParameters fd) {
		return true;
	}

	@Override
	public SeiaFeatureDetector createFD() {
		return null;
	}

	@Override
	public void normalize(SeiaKeyPoints kpt) {
	}

}
