package autoseia;

import java.util.Random;

import weka.core.DenseInstance;

public class SeiaInstance extends DenseInstance {

	private static final long serialVersionUID = 1211525160252874063L;
	
	private KeypointDescriptorList features;
	private String filename;

	public SeiaInstance(int numAttributes, int numKeypoints, String filename, SeiaImage image, Processor p, Random rnd) {
		super(numAttributes);
		this.filename = filename;
		try {
			features = p.process(image, rnd);
		} catch(Exception e) {
			throw new RuntimeException("Error processing "+filename, e);
		}
		features.resize(numKeypoints);
	}
	
	public KeypointDescriptorList getFeatures(){
		return features;
	}
	
	public String getFilename() {
		return filename;
	}
	
	@Override
	public void setValue(int attIndex, double value) {
		m_AttValues[attIndex] = value;
	}
/*	
	public void writeProcessedImage(String dst){
		Mat image = Highgui.imread(filename);
		writeProcessedImage(image, dst);
	}

	public void writeProcessedImage(Mat image, String dst) {
		Mat image2 = new Mat();
		MatOfKeyPoint mkp = new MatOfKeyPoint();
		mkp.fromList(features);
		Features2d.drawKeypoints(image, mkp, image2);
		Highgui.imwrite(dst, image2);
	}
*/	
}
