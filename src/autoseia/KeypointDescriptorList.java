package autoseia;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class KeypointDescriptorList implements Comparator<Integer>, SeiaKeyPoints{
	
	private SeiaKeyPoints keypoints;
	private SeiaDescriptors descriptors;
	private Integer[] order;
	private int size;
	
	public KeypointDescriptorList(SeiaKeyPoints keypoints, SeiaDescriptors descriptors, Random rnd) {
		this.keypoints = keypoints;
		this.descriptors = descriptors;
		size = this.keypoints.size();
		
		order = new Integer[size];
		for(int i = 0; i < size; i++)
			order[i] = i;
		shuffleArray(order, rnd);
		Arrays.sort(order, this);
	}
	
	private void shuffleArray(Integer[] ar, Random rnd) {
		if (ar.length == 0) return;
		if (rnd == null)
			rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--)
		{
			int index = rnd.nextInt(i + 1);
			// Simple swap
			Integer a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}
	
	public KeypointDescriptorList() {
		this(EmptyKeyPoints.getInstance(), EmptyDescriptors.getInstance(), null);
	}

	@Override
	public int compare(Integer o1, Integer o2) {
		
		return -Float.compare(keypoints.getResponse(o1), keypoints.getResponse(o2));
		
		/*if(keypoints.getResponse(o1) < keypoints.getResponse(o2))
			return 1;
		if(keypoints.getResponse(o1) > keypoints.getResponse(o2))
			return -1;
		return 0;*/
	}
	
	public int size(){
		return size;
	}
	
	public int descriptorSize(){
		return descriptors.numElements();
	}
	
	public double getDescriptorElement(int index, int elem){
		int i = order[index];
		return descriptors.get(i, elem);
	}

	public void resize(int numKeypoints) {
		if (numKeypoints < 0) return;
		if(numKeypoints < keypoints.size())
			size = numKeypoints;
		else
			size = keypoints.size();
	}

	@Override
	public float getSize(int index) {
		int i = order[index];
		return keypoints.getSize(i);
	}

	@Override
	public float getAngle(int index) {
		int i = order[index];
		return keypoints.getAngle(i);
	}

	@Override
	public float getResponse(int index) {
		int i = order[index];
		return keypoints.getResponse(i);
	}

	@Override
	public int getOctave(int index) {
		int i = order[index];
		return keypoints.getOctave(i);
	}

	@Override
	public int getX(int index) {
		int i = order[index];
		return keypoints.getX(i);
	}

	@Override
	public int getY(int index) {
		int i = order[index];
		return keypoints.getY(i);
	}

	@Override
	public <T> T getSpecificType(Class<T> type) {
		return null;
	}
	

}
