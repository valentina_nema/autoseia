package autoseia;

import weka.classifiers.Evaluation;
import weka.core.Instances;
import autoweka.ClassifierResult.Metric;

public class SmarterErrorRate implements Metric {
	
    public float getDefault() { 
    	return 100; 
    }
    
    public float getScore(Evaluation eval, Instances testingData){

    	int n = testingData.classAttribute().numValues();
    	for(int i=0; i<n; i++){
    		double fp = eval.numFalsePositives(i);
    		double tp = eval.numTruePositives(i);
    		if(fp + tp == n)
    			return 100;
    		if(fp > 0 || tp > 0)
    			return (float)(100 - eval.pctCorrect());
    	}
    	return (float)(100 - eval.pctCorrect());
    } 
}
