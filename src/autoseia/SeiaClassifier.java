package autoseia;

import java.util.ArrayList;

import autoseia.param.hyper.*;
import weka.attributeSelection.AttributeSelection;
import weka.classifiers.Classifier;
import weka.classifiers.SingleClassifierEnhancer;
import weka.core.Instance;
import weka.core.Instances;

public class SeiaClassifier extends SingleClassifierEnhancer{
	private static final long serialVersionUID = 4018437369166538799L;
	
	private Processor processor = new Processor();
	private SeiaInstanceFactory sif = new SeiaInstanceFactory();
	private AttributeSelection attrSelection = null;
	
	public SeiaClassifier() {
		sif.setProcessor(processor);
	}
	
	public AttributeSelection getAttributeSelection() {
		return attrSelection;
	}

	public void setAttributeSelection(AttributeSelection attrSelection) {
		this.attrSelection = attrSelection;
	}

	@Override
	public void buildClassifier(Instances data) throws Exception {
		
		Instances newData = sif.convert(data);
		if (attrSelection != null) 
			newData = attrSelection.reduceDimensionality(newData);
		m_Classifier.buildClassifier(newData);
	}

	@Override
	public double classifyInstance(Instance instance) throws Exception {
		Instances newData = sif.createDataset(0);
		Instance newInst = sif.convert(instance);
		newInst.setDataset(newData);
		if (attrSelection != null) 
			newInst = attrSelection.reduceDimensionality(newInst);
		return m_Classifier.classifyInstance(newInst);
	}
	
	@Override
	public double[] distributionForInstance(Instance instance) throws Exception {
		Instances newData = sif.createDataset(0);
		Instance newInst = sif.convert(instance);
		newInst.setDataset(newData);
		if (attrSelection != null) 
			newInst = attrSelection.reduceDimensionality(newInst);
		return m_Classifier.distributionForInstance(newInst);
	}
	
	public void setAlgorithms(Instances dataset, int nKpts, Classifier classifier, HyperParameters fdetector, HyperParameters dextractor){
		sif.setClassAttribute(dataset.classAttribute());
		sif.setNumKeypoints(nKpts);
		processor.setFeatureDetector(fdetector);
		processor.setDescriptorExtractor(dextractor);
		m_Classifier = classifier;
	}

	@Override
	public void setOptions(String[] options) throws Exception {

		ParamsReader pReader = new ParamsReader(options);
		
		sif.setNumKeypoints(pReader.getNumKeypoints());
		processor.setFeatureDetector(pReader.getFdParams());
		processor.setDescriptorExtractor(pReader.getDeParams());
		
		super.setOptions(options);
	}

	
	@Override
	public String[] getOptions() {
		String[] so = super.getOptions();
		
		if(processor.getHyperParams() == null)
			return so;
		
		HyperParameters fdParams = processor.getFeatureDetector();
		HyperParameters deParams = processor.getDescriptorExtractor();
		
		ArrayList<String> as = new ArrayList<String>();
		
		as.add("-K");
		as.add("" + sif.getNumKeypoints());
		
		as.add("-FD");
		as.add(fdParams.getClass().getName().replace("autoseia.param.hyper.", ""));
		
		as.add("-DE");
		as.add(deParams.getClass().getName().replace("autoseia.param.hyper.", ""));
		
		for (String k : fdParams.getKeys()) {
			as.add("-fd-" + k);
			as.add("" + fdParams.getParameter(k).getValue());
		}
		
		for (String k : deParams.getKeys()) {
			as.add("-de-" + k);
			as.add("" + deParams.getParameter(k).getValue());
		}
		
		for(int i = 0; i < so.length; i++){
			as.add(so[i]);
		}
		return as.toArray(new String[as.size()]);
	}
	 

}