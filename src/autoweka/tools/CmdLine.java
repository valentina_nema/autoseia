package autoweka.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import edu.emory.mathcs.util.io.ForkOutputStream;

public class CmdLine {

	public static void main(String[] args){

		if(args.length == 0){
			System.out.println("Args list empty");
			return;
		}
			
		ArrayList<String> expPath = new ArrayList<>();
		ArrayList<String> expSeed = new ArrayList<>();
		
		Scanner scan;
		try {
			scan = new Scanner(new File(args[0]));
		} catch (FileNotFoundException e1) {
			throw new RuntimeException(e1);
		}
		while(scan.hasNextLine()){
			String line = scan.nextLine();
			String[] expArgs = line.split(" ", 2);
			if(expArgs.length == 2){
				expPath.add(expArgs[0].trim());
				expSeed.add(expArgs[1].trim());
			}
		}
		scan.close();
		
		PrintStream def = System.out; 
		PrintStream err = System.err;
		System.setSecurityManager(new NoExitSecurityManager());
		
		for(int i = 0; i < expPath.size(); i++){
			FileOutputStream fos = null;
			try{
				File log = new File(expPath.get(i),"seia" + expSeed.get(i) + ".log");
				
				fos = new FileOutputStream(log);
				PrintStream foks = new PrintStream(new ForkOutputStream(def, fos));
				
				System.setOut(foks);
				System.setErr(foks);
				
				Date start = new Date();
				System.out.printf("Run started at %1$tF %1$tT %n%n", start);
				
				try {
					ExperimentRunner.main(new String[] {expPath.get(i), expSeed.get(i)});
				} catch (Exception e2) {
					err.println("Error in runner");
					e2.printStackTrace(foks);
				}
				
				Date end = new Date();
				System.out.println();
				System.out.printf("Run started at %1$tF %1$tT %n", start);
				System.out.printf("Run ended at %1$tF %1$tT %n", end);
				float time = (end.getTime() - start.getTime())/60000.f;
				System.out.println("Time elapsed " + time);
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace(err);
					}
				}
			}
		}
	}

	private static class NoExitSecurityManager extends SecurityManager 
    {
        @Override
        public void checkPermission(Permission perm) 
        {
            // allow anything.
        }
        @Override
        public void checkPermission(Permission perm, Object context) 
        {
            // allow anything.
        }
        @Override
        public void checkExit(int status) 
        {
            super.checkExit(status);
            throw new RuntimeException();
        }
    }
}
